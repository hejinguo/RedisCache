package com.darkidiot.base;

import com.darkidiot.redis.lock.Lock;
import com.darkidiot.redis.lock.RedisLock;
import lombok.extern.slf4j.Slf4j;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

/**
 * Copyright (c) 2016 成都国美大数据
 * Date:2017/7/8
 * Author: 2016年 <a href="heqiao2@gome.com.cn">darkidiot</a>
 * Desc:
 */
@Slf4j
public class TryLockTest {

    private int testCount = 2000;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Test
    public void testSimpleLock() {
        int n = testCount;
        final CountDownLatch countDownLatch = new CountDownLatch(n);
        long start = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            final Lock lock = RedisLock.useSimpleRedisLock("Simple RedisLock");
            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean b = false;
                    try {
                        b = lock.tryLock();
                        log.info(Thread.currentThread() + ":" + b);
                    } finally {
                        countDownLatch.countDown();
                        if (b) {
                            lock.unlock();
                        }
                    }
                }
            }).start();
        }
        try {
            countDownLatch.await();
            final long spendTime = System.currentTimeMillis() - start;
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    log.info("Simple RedisLock spend time " + spendTime + "ms.");
                }
            }
            ));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testStrictLock() {
        int n = testCount;
        final CountDownLatch countDownLatch = new CountDownLatch(n);
        long start = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            final Lock lock = RedisLock.useStrictRedisLock("Strict RedisLock");
            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean b = false;
                    try {
                        b = lock.tryLock();
                        log.info(Thread.currentThread() + ":" + b);
                    } finally {
                        countDownLatch.countDown();
                        if (b) {
                            lock.unlock();
                        }
                    }
                }
            }).start();
        }
        try {
            countDownLatch.await();
            final long spendTime = System.currentTimeMillis() - start;
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    log.info("Strict RedisLock spend time " + spendTime + "ms.");
                }
            }
            ));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testRigorousLock() {
        int n = testCount;
        final CountDownLatch countDownLatch = new CountDownLatch(n);
        long start = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            final Lock lock = RedisLock.useRigorousRedisLock("Rigorous RedisLock");
            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean b = false;
                    try {
                        b = lock.tryLock();
                        log.info(Thread.currentThread() + ":" + b);
                    } finally {
                        countDownLatch.countDown();
                        if (b) {
                            lock.unlock();

                        }
                    }
                }
            }).start();
        }

        try {
            countDownLatch.await();
            final long spendTime = System.currentTimeMillis() - start;
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    log.info("Rigorous RedisLock spend time " + spendTime + "ms.");
                }
            }
            ));
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }
}
